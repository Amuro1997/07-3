package com.twuc.webApp.domain;

import com.twuc.webApp.domain.composite.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class JpaTest extends JpaTestBase {

    @Autowired
    CompanyProfileRepository companyProfileRepository;

    @Autowired
    UserProfileRepository userProfileRepository;

    @Test
    public void SimpleMappingAndValueTypeTest(){
        CompanyProfile companyProfile = new CompanyProfile(new Address("AAAA","BBBB"));
        Long cid = companyProfileRepository.saveAndFlush(companyProfile).getId();

        UserProfile userProfile = new UserProfile(new Address("AAAA", "BBBB"));
        Long uid = userProfileRepository.saveAndFlush(userProfile).getId();

        assertNotNull(companyProfileRepository.findById(cid));
        assertNotNull(userProfileRepository.findById(uid));
    }
}
