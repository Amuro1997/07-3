package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity(name="company_profile")
public class CompanyProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Embedded
    Address address;

    public CompanyProfile() {
    }

    public CompanyProfile(Address address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public Address getAddress(){return address;}
}
