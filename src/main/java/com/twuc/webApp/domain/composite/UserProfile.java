package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class UserProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "city", column = @Column(name = "address_city", length = 128)),
            @AttributeOverride(name = "street", column = @Column(name = "address_street", length = 128))
    })
    Address address;

    public UserProfile() {
    }

    public Long getId() {
        return id;
    }

    public UserProfile(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }
}
